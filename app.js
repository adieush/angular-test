(function() {

    var app = angular.module("app", ['ui.bootstrap','ui.router', 'factories', 'controllers', 'directives']);
    angular.module("controllers", []);
    angular.module("directives", []);
    angular.module('factories', []);

    app.config(function($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise("/filter");

        $stateProvider
            .state('filter', {
                url: "/filter",
                templateUrl: "partials/filter.html"
            })
            .state('inputs-list', {
                url: "/inputs-list",
                templateUrl: "partials/inputs-list.html"
            });
    });

})();