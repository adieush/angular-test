angular.module('factories').factory('fields', function ($http) {
    return {
        list: function (callback) {
            $http({
                method: 'GET',
                url: 'data/data.json',
                cache: true
            }).success(callback);
        },
        filteredList: function (fields) {
            var result = [];
            angular.forEach(fields, function(value){
                if(value.visible)
                    result.push(value);
            });
            return result;
        },
        saveChanges: function (fields, field) {
            var result = [];
            angular.forEach(fields, function(value){
                if(value.name == field.name){
                    value.visible = field.visible;
                    value.value = field.value;
                }
                result.push(value);
            });
            return result;
        },
        checkedOption: function (option, field) {
            var result = false;
            for(var i= 0, len= field.value.length; i < len;i++){
                if(field.value[i] == option){
                    result = true;
                    break;
                }
            }
            return result;
        }
    }
});