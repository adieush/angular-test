angular.module("controllers").controller("SiteController", ["$scope", "fields", function($scope, fields){
    $scope.setPage = function(tab){
        $scope.page = tab;
        localStorage['tab'] = tab;
    };
    $scope.isSelected = function(tab){
        return $scope.page === tab;
    };
    $scope.filteredList = function(){
        return fields.filteredList($scope.fields);
    };
    $scope.saveVisibility = function(field){
        $scope.fields = fields.saveChanges($scope.fields, field);
    };
    $scope.saveValue = function(field){
        $scope.fields = fields.saveChanges($scope.fields, field);
    };
    $scope.checkedOption = function(option, field){
        return fields.checkedOption(option, field);
    };
    $scope.getTemplateUrl = function(directive){
        switch (directive){
            case 'email-input':
                return "partials/email-input.html";
            case 'password-input':
                return "partials/password-input.html";
            case 'birth-input':
                return "partials/birth-input.html";
            case 'gender-select':
                return "partials/gender-select.html";
            case 'language-select':
                return "partials/language-select.html";
            case 'currency':
                return "partials/currency.html";
            case 'agreement':
                return "partials/agreement.html";
            default:
                return "partials/email-input.html";

        }
    };
    $scope.init = function(){
        $scope.page = (!localStorage && localStorage['tab'] == undefined) ? 'filter' : localStorage['tab'];
        $scope.setPage($scope.page);
        fields.list(function(response){
            $scope.fields = response;
        });
    };
    $scope.init();
}]);