angular.module("directives").directive("filter", function($http, $compile){
    return{
        restrict: "E",
        transclude: true,
        templateUrl: "partials/filter.html"
    }
});