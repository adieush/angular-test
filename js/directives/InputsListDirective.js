angular.module("directives").directive("inputsList", function($http, $compile){
    return{
        restrict: "E",
        transclude: true,
        templateUrl: "partials/inputs-list.html"
    }
});